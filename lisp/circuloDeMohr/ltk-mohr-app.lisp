(defpackage :mohr-app
  (:use :common-lisp :ltk)
  (:export #:main))

(in-package :mohr-app)

(defun main()
  (setf *debug-tk* t)
  (with-ltk()
	   (let* ((frame (make-instance
		     'labelframe
		     :width 500
		     :height 200
		     :text "Transformacion de esfuerzos utilizando el circulo de Mohr"))
		 (text (make-instance
			'text
			:master frame)))
	     (pack text :side :left)
	     (setf (text text) "soy un texto")
	     (pack frame))))
		     
