(defconstant +PI+ 3.141592653589793238)

(defun rad-to-deg (rad)
  "returns the rad angle converted to degrees"
  (/ (* 180 rad) +PI+))

(defun deg-to-rad (deg)
  "returns the deg angle converted to radians"
  (/ (* +PI+ deg) 180))
