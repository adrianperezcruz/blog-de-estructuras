(load "./basicFunctions.lsp")

(defclass mohr-circle ()
  ((normal-1 :accessor normal-1
             :initarg :normal-1)
   (normal-2 :accessor normal-2
             :initarg :normal-2)
   (shear-1 :accessor shear-1
            :initarg :shear-1)
   (shear-2 :accessor shear-2))
  (:default-initargs
   :normal-1 0
   :normal-2 0
   :shear-1 0))

(defmethod initialize-instance :after
  ((circle mohr-circle) &key shear-1 &allow-other-keys)
  (setf (shear-2 circle) (* -1 shear-1))) 

(defmethod get-max-shear-stress ((circle mohr-circle))
  (let ((s (shear-1 circle))
        (n (/ (- (normal-1 circle) (normal-2 circle)) 2)))
    (sqrt (+ (expt s 2) (expt n 2)))))

(defmethod get-principal-normal-stresses ((circle mohr-circle))
  (let ((s (get-max-shear-stress circle))
        (n (/ (+ (normal-1 circle) (normal-2 circle)) 2)))
    (list (+ n s) (- n s))))

(defmethod get-stresses-at-angle ((circle mohr-circle) angle)
  (let* ((rad-angle (deg-to-rad (* 2 (- angle (first (get-principal-normal-stresses-angle circle))))))
        (mid-stress (/ (+ (normal-1 circle) (normal-2 circle)) 2))
        (s (* (get-max-shear-stress circle) (sin rad-angle)))
        (n1 (* (get-max-shear-stress circle) (cos rad-angle))))
    (list (+ n1 mid-stress) s)))

(defmethod get-principal-normal-stresses-angle ((circle mohr-circle))
  (let* ((s (shear-1 circle))
        (mid-stress (/ (- (normal-1 circle) (normal-2 circle)) 2))
        (angle-1 (* -1 (rad-to-deg (atan (/ s mid-stress) 1))))
        (angle-2 (+ angle-1 180)))
    (list (/ angle-1 2) (/ angle-2 2))))


;;; functions to use in maxima or wxMaxima
(defun $get_mohr_data (normal-1 normal-2 shear-1)
  "method defined so it can be used in maxima"
  (let ((c (make-instance 'mohr-circle :normal-1 normal-1 :normal-2 normal-2 :shear-1 shear-1)))
    (print (get-principal-normal-stresses c))
    (print (get-principal-normal-stresses-angle c))
    (print (get-max-shear-stress c))))
