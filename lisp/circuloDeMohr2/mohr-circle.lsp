(defstruct (circle (:conc-name c-))
  "Represents the mohr-circle to work with"
  (center 0 :type float :read-only t)
  (radius 0 :type float :read-only t))

(defun get-stress-radius (normal-x-stress normal-y-stress shear-stress)
  "Returns the mohr-radius from stresses"
   (let ((hor (abs (- (float normal-x-stress) (float normal-y-stress))))
	(vert (abs (* 2 (float shear-stress)))))
    (/ (sqrt (+ (* hor hor) (* vert vert))) 2)))

(defun get-stresses-at-angle (normal-x-stress normal-y-stress shear-stress grad-angle)
  "Returns stresses at the desire angle (in grads) converting angle to mohr-circle
by factored it by 2 ... positive grad-angle value es anti clock-wise"
  (if (and (eq normal-x-stress normal-y-stress) (zerop shear-stress))
      normal-x-stress
    (let* ((rad (to-radians (* grad-angle 2)))
	   (m-c (make-circle :center (/ (+ normal-x-stress normal-y-stress) 2)
			     :radius (get-stress-radius normal-x-stress normal-y-stress shear-stress)))
	   (original-rad (get-principal-angle m-c shear-stress))
	   (to-search (- rad original-rad))	 
	   (adj (* (c-radius m-c) (cos to-search)))
	   (opo (* (c-radius m-c) (sin to-search))))
    (values (+ (c-center m-c) adj) (- (c-center m-c) adj) opo))))

(defun get-principal-angle (mohr-circle shear-stress)
  "Returns the angle (in radians) where principal stresses will be found using mohr-circle"
  (asin (/ shear-stress (c-radius mohr-circle))))

(defun get-principal-stresses (normal-x-stress normal-y-stress shear-stress)
  "Returns principal stresses using mohr circle, as in max-n-stress min-n-stress max-shear-stress"
  (let ((m-c (make-circle :center (/ (+ normal-x-stress normal-y-stress) 2)
			  :radius (get-stress-radius normal-x-stress normal-y-stress shear-stress))))
    (values (+ (c-center m-c) (c-radius m-c)) (- (c-center m-c) (c-radius m-c))
	    (/ (to-grads (get-principal-angle m-c shear-stress)) 2))))

(defun to-radians (grad-angle) "converts grads to radians"
  (* grad-angle (/ PI 180)))

(defun to-grads (rad-angle) "converts radians to grad" (* rad-angle (/
								     180
								     PI)))
