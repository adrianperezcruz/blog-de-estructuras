








;; FUNCTIONS TO GET STRESSES (FLEXURAL AND SHEAR) OF RECTANGULAR BEAM
(defun flex-stress (b h y-from-centroid M)
  "Returns flexural stress of a rectangular section when a moment
M is been applied"
  (/ (* M y-from-centroid) (ix-h b h)))

(defun shear-stress (b h y-from-centroid V)
  "Returns shear stress of a rectangular section when a force
V is applied"
  (/ (* V (qx-y b h y-from-centroid)) (* b (ix-h b h))))


;; FUNCTIONS TO GET I-X AND Q-X(Y) FROM RECTANGULAR SECTION
(defun ix-h (b h)
  "Returns moment of inertia for rectangular section"
  (/ (* b h h h) 12))

(defun qx-y (b h-total y-from-centroid)
  "Returns first moment Qx measured from centroid to 
a heigth y-from-centroid"

  (let ((mid-h (/ h-total 2)))
    (if (= mid-h (abs y-from-centroid))
	0
      (let* ((d-y (- mid-h (abs y-from-centroid)))
	     (y-c (- mid-h (/ d-y 2))))
	(* b d-y y-c)))))
      






(defun ix-h (fn-bh y-c total-h &key (parts 100))
  "Returns moment of inertia I(x) of figure
for a function fn-bh of width vs height 
parts is in how many slices we divide h"

  (do* ((dy (/ total-h parts) dy)
	(slice-y-from-top (/ dy 2) (+ slice-y-from-top dy))
	(fn-slice (funcall fn-bh slice-y-from-top) (funcall fn-bh slice-y-from-top))
	(y-slice (- y-c slice-y-from-top) (- y-c slice-y-from-top))
	(i-x (* fn-slice dy y-slice y-slice) (+ i-x (* fn-slice dy y-slice y-slice))))
       ((> (+ slice-y-from-top dy) total-h) (float i-x)) 
  ))


;;(defun qx-h (fn-bh y-search y-c-top y-c-bottom total-h &key (parts 50))
;;  "Returns first moment Q(x) from total-h/2 to y-search measured
;;from y-c (centroid) so y-search most be a value in ranges
;;[-total-h/2 to total-h/2]"
;;
;;  (if (or (= y-search y-c-top) (= y-search y-c-bottom))
;;      0.0
;;    (
;;
;;     )))
